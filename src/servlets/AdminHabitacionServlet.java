package servlets;

import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import entities.Habitacion;
import entities.Reserva;
import entities.TipoHabitacion;
import entities.TipoServicio;
import entities.Usuario;
import logic.HabitacionLogic;
import logic.ReservaLogic;
import logic.TipoHabitacionLogic;
import logic.TipoServicioLogic;
import logic.UsuarioLogic;

/**
 * Servlet implementation class TarjetaServlet
 */
@WebServlet({ "/AdminHabitacion", "/adminhabitacion" })
public class AdminHabitacionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminHabitacionServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Object usuarioActual = request.getSession().getAttribute("usuarioActual");
		if (usuarioActual == null) {
			System.out.println("Usuario no loggeado. Lo mando al login");
			response.sendRedirect("login");
			return;
		}
		if (!((Usuario)usuarioActual).isAdmin()) {
			System.out.println("Usuario no admin, lo mando al home");
			response.sendRedirect("home");
			return;
		}
		// Obtener listado de habitaciones
		HabitacionLogic habitacionLogic = new HabitacionLogic();
		TipoHabitacionLogic tipoHabitacionLogic = new TipoHabitacionLogic();
		try {
			ArrayList<Habitacion> habitaciones = habitacionLogic.getAll();
			ArrayList<TipoHabitacion> tipoHabitaciones = tipoHabitacionLogic.getAll();
			
			Map<String, TipoHabitacion> tipoHabitacionPorID = new HashMap<String, TipoHabitacion>();
			for (int i=0; i<tipoHabitaciones.size(); i++)
			{
				TipoHabitacion tipoHab = tipoHabitaciones.get(i);
				int tipoHabId = tipoHab.getId();
				tipoHabitacionPorID.put(String.valueOf(tipoHabId), tipoHab);
			}

			request.setAttribute("habitaciones", habitaciones);
			request.setAttribute("tipo_habitacion_por_id", tipoHabitacionPorID);
			RequestDispatcher requestDispatcher;
			requestDispatcher = request.getRequestDispatcher("admin_habitaciones.jsp");
			requestDispatcher.forward(request, response);
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		String action = request.getParameter("action");
		String idHabitacion = request.getParameter("id_habitacion");

		if(action == null || idHabitacion == null) {
			System.out.println("No se especifica una acci�n o id de habitación");
			String alert = "No se puede procesar la acci�n";
			request.setAttribute("alert", alert);
			request.setAttribute("alert_mode", "danger");
			request.setAttribute("alert_title", "Error accediendo a la habitación");


			requestDispatcher = request.getRequestDispatcher("home.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		HabitacionLogic habitacionLogic = new HabitacionLogic();

		try {
			switch(action) {
				case "activar":
					System.out.println("Activando la habitacion: " + request.getParameter("id_habitacion"));
						habitacionLogic.activar(Integer.parseInt(request.getParameter("id_habitacion")));
					response.sendRedirect("adminhabitacion");
					return;
				case "desactivar":
					System.out.println("Desactivando la habitacion: " + request.getParameter("id_habitacion"));
						habitacionLogic.desactivar(Integer.parseInt(request.getParameter("id_habitacion")));
					response.sendRedirect("adminhabitacion");
					return;
			}
		} catch (Exception e) {
			System.out.println("Error modificando habitacion");
			e.printStackTrace();	// Si esto muestra el error en la p�gina, hay que sacarlo

			// Muestra el error general en el login
			String alert = "Hubo un error tratando de modificar la habitación. Intenta m�s tarde";
			request.setAttribute("alert", alert);
			request.setAttribute("alert_mode", "danger");
			request.setAttribute("alert_title", "Ups... ");

			requestDispatcher = request.getRequestDispatcher("admin_habitaciones.jsp");
	        requestDispatcher.forward(request, response);
	        return;
		}
		requestDispatcher = request.getRequestDispatcher("home.jsp");
		requestDispatcher.forward(request, response);
		return;
	}

}
