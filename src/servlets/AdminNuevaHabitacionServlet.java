package servlets;

import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import entities.Habitacion;
import entities.Reserva;
import entities.TipoHabitacion;
import entities.TipoServicio;
import entities.Usuario;
import logic.HabitacionLogic;
import logic.ReservaLogic;
import logic.TipoHabitacionLogic;
import logic.TipoServicioLogic;
import logic.UsuarioLogic;

/**
 * Servlet implementation class TarjetaServlet
 */
@WebServlet({ "/AdminNuevaHabitacion", "/adminnuevahabitacion" })
public class AdminNuevaHabitacionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminNuevaHabitacionServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Object usuarioActual = request.getSession().getAttribute("usuarioActual");
		if (usuarioActual == null) {
			System.out.println("Usuario no loggeado. Lo mando al login");
			response.sendRedirect("login");
			return;
		}
		if (!((Usuario)usuarioActual).isAdmin()) {
			System.out.println("Usuario no admin, lo mando al home");
			response.sendRedirect("home");
			return;
		}

		TipoHabitacionLogic tipoHabitacionLogic = new TipoHabitacionLogic();
		try {
			ArrayList<TipoHabitacion> tipoHabitaciones = tipoHabitacionLogic.getAll();

			request.setAttribute("tipo_habitaciones", tipoHabitaciones);
			RequestDispatcher requestDispatcher;
			requestDispatcher = request.getRequestDispatcher("admin_nueva_habitacion.jsp");
			requestDispatcher.forward(request, response);
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		HabitacionLogic habitacionLogic = new HabitacionLogic();

		System.out.println("Creando una habitacion");
		try {
			int numero = Integer.parseInt(request.getParameter("numero"));
			int idTipoHab = Integer.parseInt(request.getParameter("id_tipo_habitacion"));
			Habitacion nuevaHabitacion = new Habitacion();
			nuevaHabitacion.setNumero(numero);
			nuevaHabitacion.setIdTipoHabitacion(idTipoHab);
			nuevaHabitacion.setActiva(true);
			habitacionLogic.create(nuevaHabitacion);
			response.sendRedirect("adminhabitacion");
			return;
		} catch (Exception e) {
			System.out.println("Error creando habitacion");
			e.printStackTrace();	// Si esto muestra el error en la p�gina, hay que sacarlo

			// Muestra el error general en el login
			String alert = "Hubo un error tratando de crear la habitación. Intenta m�s tarde";
			request.setAttribute("alert", alert);
			request.setAttribute("alert_mode", "danger");
			request.setAttribute("alert_title", "Ups... ");

			requestDispatcher = request.getRequestDispatcher("admin_habitaciones.jsp");
	        requestDispatcher.forward(request, response);
	        return;
		}
	}

}
