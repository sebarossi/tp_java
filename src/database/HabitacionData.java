package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import entities.Habitacion;
import entities.Reserva;
import entities.Servicio;
import entities.TipoHabitacion;


public class HabitacionData {

	public Habitacion getOne(int id_habitacion) throws Exception{

		Statement stmt=null;
		ResultSet rs=null;
		Habitacion hab = new Habitacion();

		try{
			stmt = FactoryConection.getInstancia()
					.getConn().createStatement();
			rs = stmt.executeQuery("select * from habitaciones where id_habitacion = '" + id_habitacion + "'");
			if(rs!=null){
				while(rs.next()){
					hab.setId(rs.getInt("id_habitacion"));
					hab.setNumero(rs.getInt("numero"));
					hab.setIdTipoHabitacion(rs.getInt("id_tipo_habitacion"));
					hab.setIdReserva(rs.getInt("id_reserva"));
					hab.setActiva(rs.getBoolean("activa"));
				}
			}
		} catch (Exception e){
			throw e;
		}

		try {
			if(rs!=null) rs.close();
			if(stmt!=null) stmt.close();
			FactoryConection.getInstancia().releaseConn();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return hab;
	}


	public ArrayList<Habitacion> getAll() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Habitacion> habitaciones = new ArrayList<Habitacion>();

		try {
			stmt = FactoryConection.getInstancia().getConn().createStatement();
			rs = stmt.executeQuery("select * from habitaciones");
			if (rs != null) {
				while (rs.next()) {
					Habitacion hab = new Habitacion();

					hab.setId(rs.getInt("id_habitacion"));
					hab.setNumero(rs.getInt("numero"));
					hab.setIdTipoHabitacion(rs.getInt("id_tipo_habitacion"));
					hab.setIdReserva(rs.getInt("id_reserva"));
					hab.setActiva(rs.getBoolean("activa"));

					habitaciones.add(hab);

				}
			}
		} catch (Exception e) {
			System.out.println("Error getAll Habitacion");
			throw e;
		}

		try {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
			FactoryConection.getInstancia().releaseConn();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return habitaciones;
	}
	
	public void create(Habitacion habitacion) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		int idHabitacion = 0;

		try {
			stmt = FactoryConection.getInstancia().getConn().createStatement();
			stmt.executeUpdate(
					"insert into habitaciones (numero, id_tipo_habitacion, activa) values ("
							+ habitacion.getNumero() + ", " + habitacion.getIdTipoHabitacion() + ", "
							+ (habitacion.getActiva() ? 1 : 0) + ");",
					Statement.RETURN_GENERATED_KEYS);

		} catch (Exception e) {
			System.out.println("Error al crear la habitación en la DB");
			throw e;
		}
	}

	public int getCantidadPorTipo(int idTipo) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		int cantidad = 0;

		try {
			stmt = FactoryConection.getInstancia().getConn().createStatement();
			rs = stmt.executeQuery(
					"select count(*) as 'cantidad' from habitaciones where id_tipo_habitacion = '" + idTipo + "'"
			+ " AND activa=1;");
			if (rs != null) {
				while (rs.next()) {
					cantidad = rs.getInt("cantidad");
				}
			}
		} catch (Exception e) {
			throw e;
		}

		try {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
			FactoryConection.getInstancia().releaseConn();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return cantidad;
	}

	public ArrayList<Habitacion> getFromReserva(int idReserva) throws Exception {

		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Habitacion> habitaciones = new ArrayList<Habitacion>();

		try {
			stmt = FactoryConection.getInstancia().getConn().createStatement();
			rs = stmt.executeQuery("select * from habitaciones where id_reserva = '" + idReserva+ "';");

			if (rs != null) {
				while (rs.next()) {
					Habitacion th = new Habitacion();

					th.setId(rs.getInt("id_habitacion"));
					th.setIdTipoHabitacion(rs.getInt("id_tipo_habitacion"));
					if(rs.getString("id_reserva") != "") {
						th.setIdReserva(rs.getInt("id_reserva"));
					}
					th.setNumero(rs.getInt("numero"));

					habitaciones.add(th);

				}
			}
		} catch (Exception e) {
			throw e;
		}

		try {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
			FactoryConection.getInstancia().releaseConn();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return habitaciones;
	}
	
	public ArrayList<Habitacion> getFromServicios(ArrayList<Servicio> servicios) throws Exception {

		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Habitacion> habitaciones = new ArrayList<Habitacion>();

		try {
			
			String idsHabitacionesString = "(";
			for(int i = 0; i < servicios.size(); i++) {
				idsHabitacionesString += String.valueOf(servicios.get(i).getIdHabitacion());
				if(i != servicios.size()-1)
					idsHabitacionesString += ", ";
			}
			idsHabitacionesString += ")";

			stmt = FactoryConection.getInstancia().getConn().createStatement();
			rs = stmt.executeQuery("select * from habitaciones where id_habitacion IN " + idsHabitacionesString + ";");

			if (rs != null) {
				while (rs.next()) {
					Habitacion th = new Habitacion();

					th.setId(rs.getInt("id_habitacion"));
					th.setIdTipoHabitacion(rs.getInt("id_tipo_habitacion"));
					if(rs.getString("id_reserva") != "") {
						th.setIdReserva(rs.getInt("id_reserva"));
					}
					th.setNumero(rs.getInt("numero"));

					habitaciones.add(th);

				}
			}
		} catch (Exception e) {
			throw e;
		}

		try {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
			FactoryConection.getInstancia().releaseConn();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return habitaciones;
	}

	public ArrayList<Habitacion> getDisponiblesPorTipo(int idTipoHabitacion) throws Exception {

		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Habitacion> habitaciones = new ArrayList<Habitacion>();

		try {
			stmt = FactoryConection.getInstancia().getConn().createStatement();
			rs = stmt.executeQuery("select * from habitaciones where id_reserva IS NULL AND id_tipo_habitacion = '" + idTipoHabitacion + "';");

			if (rs != null) {
				while (rs.next()) {
					Habitacion th = new Habitacion();

					th.setId(rs.getInt("id_habitacion"));
					th.setIdTipoHabitacion(rs.getInt("id_tipo_habitacion"));
					if(rs.getString("id_reserva") != "") {
						th.setIdReserva(rs.getInt("id_reserva"));
					}
					th.setNumero(rs.getInt("numero"));

					habitaciones.add(th);

				}
			}
		} catch (Exception e) {
			throw e;
		}

		try {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
			FactoryConection.getInstancia().releaseConn();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return habitaciones;
	}


	public void reservar(String idString, int idReserva) throws Exception {
		Statement stmt = null;
		try {
			stmt = FactoryConection.getInstancia().getConn().createStatement();
			String query = "UPDATE habitaciones SET id_reserva = '" + idReserva + "'";
			query += " WHERE (id_habitacion IN (" + idString + "))";

			stmt.executeUpdate(query);

		} catch (Exception e) {
			System.out.println("Error al reservar habitaciones" + idString);
			throw e;
		}
	}

	public void liberar(String idString) throws Exception {
		Statement stmt = null;
		try {
			stmt = FactoryConection.getInstancia().getConn().createStatement();
			String query = "UPDATE habitaciones SET id_reserva = NULL";
			query += " WHERE (id_habitacion IN (" + idString + "))";

			stmt.executeUpdate(query);

		} catch (Exception e) {
			System.out.println("Error al liberar habitaciones" + idString);
			throw e;
		}
	}


	public void activar(int idHabitacion) throws Exception {
		Statement stmt = null;
		try {
			stmt = FactoryConection.getInstancia().getConn().createStatement();
			String query = "UPDATE habitaciones SET activa = 1";
			query += " WHERE (id_habitacion =" + idHabitacion + ")";

			stmt.executeUpdate(query);

		} catch (Exception e) {
			System.out.println("Error al activar habitacion" + idHabitacion);
			throw e;
		}
	}

	public void desactivar(int idHabitacion) throws Exception {
		Statement stmt = null;
		try {
			stmt = FactoryConection.getInstancia().getConn().createStatement();
			String query = "UPDATE habitaciones SET activa = 0";
			query += " WHERE (id_habitacion =" + idHabitacion + ")";

			stmt.executeUpdate(query);

		} catch (Exception e) {
			System.out.println("Error al desactivar habitacion" + idHabitacion);
			throw e;
		}
	}
}
