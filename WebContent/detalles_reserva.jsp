<%@include file="./master_header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.util.Map" %>
<%@ page import="entities.Reserva"%>
<%@ page import="entities.TipoServicio"%>
<%@ page import="entities.Servicio"%>
<%@ page import="entities.Habitacion"%>
<%@ page import="java.text.SimpleDateFormat"%>

<script>
	document.title = "Arroz Tower - Detalles de reserva";	
</script>

<%
	int resId = (int) request.getAttribute("resId");
	ArrayList<Habitacion> habitaciones = (ArrayList<Habitacion>) request.getAttribute("habitaciones");
	Map<String, TipoServicio> tipoServicioPorId = (Map<String, TipoServicio>) request.getAttribute("tipoServicioPorId");
	ArrayList<Servicio> servicios = (ArrayList<Servicio>) request.getAttribute("servicios_pedidos");
	if(servicios == null || servicios.isEmpty()){
%>
<h2 class="h1-responsive font-weight-bold text-center my-4">No hay
	ningun servicio para esta reserva</h2>
<%
	} else{
%>
<div class="row mt-3">
	<div class="col-md-10 offset-md-1">
		<table class="table">
			<thead>
				<tr>
					<th scope="col">Num. hab.</th>
					<th scope="col">Servicio</th>
					<th scope="col">Cantidad</th>
					<th scope="col">Total</th>
				</tr>
			</thead>
			<tbody>
				<% for (int servCounter = 0; servCounter < servicios.size(); servCounter ++) {
					Servicio servicio = servicios.get(servCounter);
					float precio = 0;
					String nombreServicio = "-";
					if(tipoServicioPorId.containsKey(String.valueOf(servicio.getIdTipoServicio()))) {
						TipoServicio tipoServicio = tipoServicioPorId.get(String.valueOf(servicio.getIdTipoServicio()));
						nombreServicio = tipoServicio.getNombre().toString();
						precio = servicio.getCantidad() * tipoServicio.getPrecio();
					}
					%>
					<tr>
						<td colspan>
							<%= servicio.getIdHabitacion() %>
						</td>
						<td colspan>
							<%= nombreServicio %>
						</td>
						<td colspan>
							<%= servicio.getCantidad() %>
						</td>
						<td colspan>
							<%= precio %>
						</td>
					</tr>
				<% } %>
			</tbody>
		</table>
	</div>
</div>


<%
	}
%>




<%@include file="./master_footer.jsp"%>
