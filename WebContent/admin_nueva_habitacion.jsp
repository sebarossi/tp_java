<%@include file="./master_header.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page import="java.util.Map" %>
<%@ page import="entities.Habitacion" %>
<%@ page import="entities.Usuario" %>
<%@ page import="entities.TipoHabitacion" %>

<link href=".\bootstrap_login\bootstrap_login.css" rel="stylesheet">
<script src=".\bootstrap_login\bootstrap_login.js"></script>

<script>
document.title = "Arroz Tower - Habitaciones";
 </script>
 
<div class="row mt-3">
	<div class="col-md-4 offset-md-4">
		<%
			ArrayList<TipoHabitacion> tipoHabitaciones = (ArrayList<TipoHabitacion>)request.getAttribute("tipo_habitaciones");
			if(tipoHabitaciones.isEmpty()){
		%>
			<h2 class="h1-responsive font-weight-bold text-center my-4">No hay ninguna tipo de habitación para mostrar</h2>
			<div class="d-flex justify-content-around my-3">
				<form action="home" method="post" role="form">
					<button type="submit" name="submit" value="submit-reserva" class="btn btn-warning" style="float: right">
						<a href="adminhabitacion" style="text-decoration: none; color: #000000b3; ">Volver</a>
					</button>
				</form>
			</div>
		<%
				} else{
		%>
			<div class="card card-signin flex-row my-5">
				<div class="card-body">
				<form class="cool-form" action="adminnuevahabitacion" method="post" role="form">
					<div class="form-label-group">
						<input type="text" name="numero" tabindex="1" class="form-control" placeholder="Número" value="">
					</div>
					<div class="form-label-group">
						<select name="id_tipo_habitacion" tabindex="2" class="form-control">
						<%
						for(int i = 0; i < tipoHabitaciones.size(); i++){
							TipoHabitacion tipoHabitacion = tipoHabitaciones.get(i);
							%>
								<option value="<%= tipoHabitacion.getId() %>"><%= tipoHabitacion.getTipoHabitacion().toString() %></option>
							<%
						}
						 %>
						</select>
					</div>
					<input type="submit" tabindex="3" class="btn btn-lg btn-primary btn-block text-uppercase" value="Crear">
		
				</form>
			</div>
		</div>
		<% } %>
	</div>
</div>



<%@include file="./master_footer.jsp" %>
