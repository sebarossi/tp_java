<%@include file="./master_header.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page import="java.util.Map" %>
<%@ page import="entities.Habitacion" %>
<%@ page import="entities.Usuario" %>
<%@ page import="entities.TipoHabitacion" %>

<script>
document.title = "Arroz Tower - Habitaciones";
 </script>
<div class="row mt-3">
	<div class="col-md-10 offset-md-1">
	<%
		Map<String, TipoHabitacion> tipoHabitacionPorId = (Map<String, TipoHabitacion>)request.getAttribute("tipo_habitacion_por_id");
		ArrayList<Habitacion> habitaciones = (ArrayList<Habitacion>)request.getAttribute("habitaciones");
			if(habitaciones.isEmpty()){
	%>
		<h2 class="h1-responsive font-weight-bold text-center my-4">No hay ninguna habitación para mostrar</h2>
	<%
			} else{
	%>
		<div class="d-flex justify-content-right my-3">
			<button type="submit" name="submit" value="submit-reserva" class="btn btn-info" style="float: right">
				<a href="adminnuevahabitacion" style="text-decoration: none; color: #000000b3; ">Crear habitación</a>
			</button>
		</div>
		<table class="table">
			<thead>
				<tr>
					<th scope="col">Número</th>
					<th scope="col">Tipo</th>
					<th scope="col">Estado</th>
					<th scope="col">ID Reserva</th>
					<th scope="col">Acciones</th>
				</tr>
			</thead>
			<tbody>
				<%
				for(int i = 0; i < habitaciones.size(); i++){
					Habitacion habitacion = habitaciones.get(i);
					boolean reservada = habitacion.getIdReserva() != 0;
					String clase = !habitacion.getActiva() ? "table-danger" : reservada ? "table-info" : "table-primary";
					String estado = !habitacion.getActiva() ? "Desactivada" : reservada ? "Reservada" : "Disponible";
					String nombreTipoHab = "";
					if(tipoHabitacionPorId.containsKey(String.valueOf(habitacion.getIdTipoHabitacion()))) {
						nombreTipoHab = tipoHabitacionPorId.get(String.valueOf(habitacion.getIdTipoHabitacion())).getTipoHabitacion().toString();
					}
					%>
						<tr class="<%= clase %>">
							<td class="cell-numero"><%= habitacion.getNumero() %></td>
							<td class="tipo"><%= nombreTipoHab %></td>
							<td class="cell-estado"><%= estado %></td>
							<td class="cell-id-reserva"><%= habitacion.getIdReserva() == 0 ? "-" : habitacion.getIdReserva() %></td>
							<td class="cell-acciones">
								<% if(habitacion.getActiva() && !reservada || !habitacion.getActiva()) { %>
									<form id="actualizar_habitacion-form" name="actualizar_habitacion-form" action="adminhabitacion" method="POST">
										<div class="btn-group btn-group-sm" role="group">
										<input type="hidden" name="id_habitacion" value="<%= habitacion.getId() %>">
										<% if(habitacion.getActiva() && !reservada) { %>
											<button type="submit" name="action" value="desactivar" class="btn-desactivar-<%= habitacion.getId() %> btn btn-outline-danger">Desactivar</button>
										<% } else { %>
											<button type="submit" name="action" value="activar" class="btn-activar-<%= habitacion.getId() %> btn btn-outline-success">Activar</button>
										<% } %>
										</div>
									</form>
								<% } else { %>
									<p><strong>-</strong></p>
								<% } %>
							</td>
						</tr>
					<%
				}
			}
				%>
			</tbody>
		</table>
	</div>
</div>



<%@include file="./master_footer.jsp" %>
